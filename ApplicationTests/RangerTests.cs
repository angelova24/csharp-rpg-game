﻿using RPG_Characters.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ApplicationTests
{
    public class RangerTests
    {
        public Ranger ranger = new Ranger("myNewRanger");
        [Fact]
        public void CreateRanger_ShouldSetNameCorrectly()
        {
            string expected = "myNewRanger";

            string actual = ranger.Name;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateRanger_ShouldSetLevelToOne()
        {
            int expected = 1;

            int actual = ranger.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateRanger_ShouldSetStrengthCorrecty()
        {
            double expected = 1;

            double actual = ranger.CharacterBaseAttributes.Strength;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateRanger_ShouldSetDexterityCorrecty()
        {
            double expected = 7;

            double actual = ranger.CharacterBaseAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateRanger_ShouldSetIntelligenceCorrecty()
        {
            double expected = 1;

            double actual = ranger.CharacterBaseAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseLevelByOne()
        {
            ranger.LevelUp();

            double expected = 2;

            double actual = ranger.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeStrengthByOne()
        {
            ranger.LevelUp();

            double expected = 2;

            double actual = ranger.CharacterBaseAttributes.Strength;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeDexterityByFive()
        {
            ranger.LevelUp();

            double expected = 12;

            double actual = ranger.CharacterBaseAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeIntelligenceByOne()
        {
            ranger.LevelUp();

            double expected = 2;

            double actual = ranger.CharacterBaseAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }
    }
}
