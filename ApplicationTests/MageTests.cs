using RPG_Characters.Character;
using RPG_Characters.Equipment;
using System;
using Xunit;

namespace ApplicationTests
{
    public class MageTests
    {
        public Mage mage = new Mage("myNewMage");
        [Fact]
        public void CreateMage_ShouldSetNameCorrectly()
        {
            string expected = "myNewMage";

            string actual = mage.Name;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateMage_ShouldSetLevelToOne()
        {
            int expected = 1;

            int actual = mage.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateMage_ShouldSetStrengthCorrecty()
        {
            double expected = 1;

            double actual = mage.CharacterBaseAttributes.Strength;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateMage_ShouldSetDexterityCorrecty()
        {
            double expected = 1;

            double actual = mage.CharacterBaseAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateMage_ShouldSetIntelligenceCorrecty()
        {
            double expected = 8;

            double actual = mage.CharacterBaseAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseLevelByOne()
        {
            mage.LevelUp();

            double expected = 2;

            double actual = mage.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeStrengthByOne()
        {
            mage.LevelUp();

            double expected = 2;

            double actual = mage.CharacterBaseAttributes.Strength;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeDexterityByOne()
        {
            mage.LevelUp();

            double expected = 2;

            double actual = mage.CharacterBaseAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeIntelligenceByFive()
        {
            mage.LevelUp();

            double expected = 13;

            double actual = mage.CharacterBaseAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }
    }
}
