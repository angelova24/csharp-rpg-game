﻿using RPG_Characters.Character;
using RPG_Characters.Equipment;
using RPG_Characters.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ApplicationTests
{
    public class ItemsAndEquipmentTests
    {
        public Warrior warrior = new Warrior("myNewWarrior");
        public Weapon staffLevel1 = new Weapon("myFirstStaff", 1, WeaponType.WEAPON_STAFF, 7, 1.1);
        public Weapon staffLevel2 = new Weapon("mySecondStaff", 2, WeaponType.WEAPON_STAFF, 7, 1.1);
        public Weapon axeLevel1 = new Weapon("myFirstAxe", 1, WeaponType.WEAPON_AXE, 7, 1.1);
        public Weapon axeLevel2 = new Weapon("mySecondAxe", 2, WeaponType.WEAPON_AXE, 7, 1.1);
        public Weapon bow = new Weapon("mySecondAxe", 2, WeaponType.WEAPON_BOW, 7, 1.1);
        public Armour bodyClothLevel1 = new Armour("myFirstArmour", 1, ArmourType.ARMOUR_CLOTH, SlotType.BODY);
        public Armour bodyClothLevel2 = new Armour("mySecondArmour", 2, ArmourType.ARMOUR_CLOTH, SlotType.BODY);
        public Armour legsCloth = new Armour("myLegsArmour", 1, ArmourType.ARMOUR_CLOTH, SlotType.LEGS);
        public Armour bodyMail = new Armour("myBodyMail", 2, ArmourType.ARMOUR_MAIL, SlotType.BODY);
        public Armour legsMail = new Armour("myLegsMail", 1, ArmourType.ARMOUR_MAIL, SlotType.LEGS);
        public Armour bodyPlateLevel1 = new Armour("myBodyPlate", 1, ArmourType.ARMOUR_PLATE, SlotType.BODY);
        public Armour bodyPlateLevel2 = new Armour("myBodyPlate", 2, ArmourType.ARMOUR_PLATE, SlotType.BODY);

        [Fact]
        public void SetWeapon_SetWeaponWithTooHighLevel_ShouldThrowInvalidWeaponException()
        {
            Assert.Throws<InvalidWeaponException>(() => warrior.SetWeapon(axeLevel2));
        }
        [Fact]
        public void SetWeapon_SetNotAllowedWeapon_ShouldThrowInvalidWeaponException()
        {
            Assert.Throws<InvalidWeaponException>(() => warrior.SetWeapon(bow));
        }
        [Fact]
        public void SetWeapon_SetAllowedWeapon_ShouldReturnMessage()
        {
            string expected = "New weapon equipped!";

            string actual = warrior.SetWeapon(axeLevel1);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void SetArmour_SetArmourWithTooHighLevel_ShouldThrowInvalidArmourException()
        {
            Assert.Throws<InvalidArmourException>(() => warrior.SetArmour(bodyPlateLevel2));
        }
        [Fact]
        public void SetArmour_SetNotAllowedArmour_ShouldThrowInvalidArmourException()
        {
            Assert.Throws<InvalidArmourException>(() => warrior.SetArmour(legsCloth));
        }
        [Fact]
        public void SetArmour_SetAllowedArmour_ShouldReturnMessage()
        {
            string expected = "New armour equipped!";

            string actual = warrior.SetArmour(bodyPlateLevel1);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateTotalAttributes_SetArmourToIncreaseStrength_ShouldReturnCorrectAmountIntelligence()
        {
            bodyPlateLevel1.ItemAttributes.Strength = 5;
            legsMail.ItemAttributes.Strength = 7;
            warrior.SetArmour(bodyPlateLevel1);
            warrior.SetArmour(legsMail);
            double expected = 17;

            warrior.CalculateTotalAttributes();
            double actual = warrior.TotalAttributes.Strength;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDamage_WhitoutWeapon_ShouldReturnCorrectDamage()
        {
            double expected = 1 * (1 + (5 / 100));

            double actual = warrior.CalculateDamage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDamage_NextLevelWithoutEquipment_ShouldReturnCorrectDamage()
        {
            warrior.LevelUp();
            double expected = 1 * (1 + ((5 + 3) / 100));

            double actual = warrior.CalculateDamage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDamage_WhithWeapon_ShouldReturnCorrectDamage()
        {
            warrior.SetWeapon(axeLevel1);
            double expected = Math.Round((7 * 1.1) * (1 + (5 / 100)), 2);

            double actual = warrior.CalculateDamage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDamage_WhithWeaponAndArmour_ShouldReturnCorrectDamage()
        {
            bodyPlateLevel1.ItemAttributes.Strength = 1;
            warrior.SetWeapon(axeLevel1);
            warrior.SetArmour(bodyPlateLevel1);

            double expected = Math.Round((7 * 1.1) * (1 + ((5 + 1) / 100)), 2);

            double actual = warrior.CalculateDamage();

            Assert.Equal(expected, actual);
        }

    }
}