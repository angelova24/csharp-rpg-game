﻿using RPG_Characters.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ApplicationTests
{
    public class WarriorTests
    {
        public Warrior warrior = new Warrior("myNewWarrior");
        [Fact]
        public void CreateWarrior_ShouldSetNameCorrectly()
        {
            string expected = "myNewWarrior";

            string actual = warrior.Name;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateWarrior_ShouldSetLevelToOne()
        {
            int expected = 1;

            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateWarrior_ShouldSetStrengthCorrecty()
        {
            double expected = 5;

            double actual = warrior.CharacterBaseAttributes.Strength;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateWarrior_ShouldSetDexterityCorrecty()
        {
            double expected = 2;

            double actual = warrior.CharacterBaseAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateWarrior_ShouldSetIntelligenceCorrecty()
        {
            double expected = 1;

            double actual = warrior.CharacterBaseAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseLevelByOne()
        {
            warrior.LevelUp();

            double expected = 2;

            double actual = warrior.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeStrengthByThree()
        {
            warrior.LevelUp();

            double expected = 8;

            double actual = warrior.CharacterBaseAttributes.Strength;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeDexterityByTwo()
        {
            warrior.LevelUp();

            double expected = 4;

            double actual = warrior.CharacterBaseAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeIntelligenceByOne()
        {
            warrior.LevelUp();

            double expected = 2;

            double actual = warrior.CharacterBaseAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }
    }
}
