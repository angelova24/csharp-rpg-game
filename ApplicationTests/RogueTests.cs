﻿using RPG_Characters.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ApplicationTests
{
    public class RogueTests
    {
        public Rogue rogue = new Rogue("myNewRogue");
        [Fact]
        public void CreateRogue_ShouldSetNameCorrectly()
        {
            string expected = "myNewRogue";

            string actual = rogue.Name;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateRogue_ShouldSetLevelToOne()
        {
            int expected = 1;

            int actual = rogue.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateRogue_ShouldSetStrengthCorrecty()
        {
            double expected = 2;

            double actual = rogue.CharacterBaseAttributes.Strength;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateRogue_ShouldSetDexterityCorrecty()
        {
            double expected = 6;

            double actual = rogue.CharacterBaseAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CreateRogue_ShouldSetIntelligenceCorrecty()
        {
            double expected = 1;

            double actual = rogue.CharacterBaseAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseLevelByOne()
        {
            rogue.LevelUp();

            double expected = 2;

            double actual = rogue.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeStrengthByOne()
        {
            rogue.LevelUp();

            double expected = 3;

            double actual = rogue.CharacterBaseAttributes.Strength;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeDexterityByFour()
        {
            rogue.LevelUp();

            double expected = 10;

            double actual = rogue.CharacterBaseAttributes.Dexterity;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_ShouldIncreaseAttributeIntelligenceByOne()
        {
            rogue.LevelUp();

            double expected = 2;

            double actual = rogue.CharacterBaseAttributes.Intelligence;

            Assert.Equal(expected, actual);
        }
    }
}
