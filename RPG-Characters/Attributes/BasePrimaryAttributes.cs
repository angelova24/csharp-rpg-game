﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Attributes
{
    public class BasePrimaryAttributes
    {
        public double Strength { get; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }

        /// <summary>
        /// Increase the base attributes of the character
        /// </summary>
        /// <param name="strength">The amount of strength you want to add to the characters strength</param>
        /// <param name="dexterity">The amount of dexterity you want to add to the characters dexterity</param>
        /// <param name="intelligence">The amount of intelligence you want to add to the characters intelligence</param>
        public void IncreaseBaseAttributes(double strength, double dexterity, double intelligence)
        {
            Strength += strength;
            Dexterity += dexterity;
            Intelligence += intelligence;
        }
    }
}
