﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Exception
{
    public class InvalidWeaponException : SystemException
    {
        public InvalidWeaponException(string message) : base(message)
        {
        }
    }
}
