﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Exception
{
    public class InvalidArmourException : SystemException
    {
        public InvalidArmourException(string message) : base(message)
        {
        }
    }
}
