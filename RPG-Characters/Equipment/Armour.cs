﻿using RPG_Characters.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Equipment
{
    public class Armour : Item
    {
        private SlotType slotType;

        public Armour(string name, int requiredLevel, ArmourType armourType, SlotType slotType) : base(name, requiredLevel)
        {
            ArmourType = armourType;
            SlotType = slotType;
        }

        public ArmourType ArmourType { get; protected set; }
        public SlotType SlotType
        {
            get => slotType;
            set
            {
                if (value == SlotType.WEAPON)
                {
                    throw new InvalidArmourException("Invalid slot");
                }
                else
                {
                    slotType = value;
                }
            }
        }

    }
}
