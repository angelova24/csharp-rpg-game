﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Equipment
{
    public class Weapon : Item
    {
        public Weapon(string name, int requiredLevel, WeaponType weaponType, int damage, double attack) : base(name, requiredLevel)
        {
            WeaponType = weaponType;
            Damage = damage;
            AttackSpeed = attack;
        }

        public WeaponType WeaponType { get; set; }
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }

        /// <summary>
        /// Calculate the weapons damage per second
        /// </summary>
        /// <returns>The damage</returns>
        public double CalculateDPS()
        {
            return Math.Round(Damage * AttackSpeed, 2);
        }
    }
}
