﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Equipment
{
    public enum SlotType
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
}
