﻿using RPG_Characters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Equipment
{
    public abstract class Item
    {
        public Item(string name, int requiredLevel)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            ItemAttributes = new BasePrimaryAttributes();
        }
        public string Name { get; protected set; }
        public int RequiredLevel { get; protected set; }
        public BasePrimaryAttributes ItemAttributes { get; protected set; }
    }
}
