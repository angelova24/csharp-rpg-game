﻿using RPG_Characters.Character;
using RPG_Characters.Equipment;
using System;

namespace RPG_Characters
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Mage mage = new Mage("My mage");
            Ranger ranger = new Ranger("My ranger");

            Weapon staff = new Weapon("common staff", 1, WeaponType.WEAPON_STAFF, 8, 1.6);
            Weapon bow = new Weapon("common bow", 1, WeaponType.WEAPON_BOW, 12, 0.8);

            Armour headCloth = new Armour("common cloth", 1, ArmourType.ARMOUR_CLOTH, SlotType.HEAD);
            Armour bodyLeather = new Armour("common leather", 1, ArmourType.ARMOUT_LEATHER, SlotType.BODY);

            headCloth.ItemAttributes.Intelligence = 3;
            headCloth.ItemAttributes.Strength = 2;
            bodyLeather.ItemAttributes.Dexterity = 4;
            bodyLeather.ItemAttributes.Intelligence = 3;

            Console.WriteLine("-------Mage level 1-------");
            Console.WriteLine(mage.GetStats());
            Console.WriteLine("-------Ranger level 1-------");
            Console.WriteLine(ranger.GetStats());

            mage.LevelUp();
            ranger.LevelUp();

            Console.WriteLine("_____________________________");
            Console.WriteLine("-------Mage level 2-------");
            Console.WriteLine(mage.GetStats());
            Console.WriteLine("-------Ranger level 2-------");
            Console.WriteLine(ranger.GetStats());

            mage.SetArmour(headCloth);
            ranger.SetArmour(bodyLeather);

            Console.WriteLine("_____________________________");
            Console.WriteLine("-------Mage level 2 with armour-------");
            Console.WriteLine(mage.GetStats());
            Console.WriteLine("-------Ranger level 2 with armour-------");
            Console.WriteLine(ranger.GetStats());

            mage.SetWeapon(staff);
            ranger.SetWeapon(bow);

            Console.WriteLine("_____________________________");
            Console.WriteLine("-------Mage level 2 with armour & weapon-------");
            Console.WriteLine(mage.GetStats());
            Console.WriteLine("-------Ranger level 2 with armour & weapon-------");
            Console.WriteLine(ranger.GetStats());
        }
    }
}
