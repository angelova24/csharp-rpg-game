﻿using RPG_Characters.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Character
{
    public class Mage : Character
    {

        public Mage(string name) : base(name)
        {
            CharacterBaseAttributes.Strength = 1;
            CharacterBaseAttributes.Dexterity = 1;
            CharacterBaseAttributes.Intelligence = 8;
            AllowedWeapons = new WeaponType[] { WeaponType.WEAPON_STAFF, WeaponType.WEAPON_WAND };
            AllowedArmours = new ArmourType[] { ArmourType.ARMOUR_CLOTH };
        }

        public override double CalculateDamage()
        {
            return Math.Round(WeaponDPS * (1 + TotalAttributes.Intelligence / 100), 2);
        }

        public override void LevelUp()
        {
            base.LevelUp();
            CharacterBaseAttributes.IncreaseBaseAttributes(1, 1, 5);
        }
    }
}
