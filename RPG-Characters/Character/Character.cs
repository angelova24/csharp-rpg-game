﻿using RPG_Characters.Attributes;
using RPG_Characters.Equipment;
using RPG_Characters.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Character
{
    public abstract class Character : ICharacter
    {
        /// <summary>
        /// Create new character
        /// </summary>
        /// <param name="name">The name of the character</param>
        protected Character(string name)
        {
            Name = name;
            Level = 1;
            WeaponDPS = 1;
            CharacterBaseAttributes = new BasePrimaryAttributes();
            ExtraAttributes = new BasePrimaryAttributes();
            TotalAttributes = new BasePrimaryAttributes();
            ExtraAttributes = new BasePrimaryAttributes();
            Equipment = new Dictionary<SlotType, Item>();
        }

        public string Name { get; set; }
        public int Level { get; set; }
        public BasePrimaryAttributes CharacterBaseAttributes { get; set; }
        public BasePrimaryAttributes ExtraAttributes { get; set; }
        public BasePrimaryAttributes TotalAttributes { get; set; }
        public WeaponType[] AllowedWeapons { get; set; }
        public ArmourType[] AllowedArmours { get; set; }
        public double WeaponDPS { get; set; }
        public Dictionary<SlotType, Item> Equipment { get; set; }

        /// <summary>
        /// Get to next level
        /// </summary>
        public virtual void LevelUp()
        {
            Level++;
        }

        /// <summary>
        /// Calculate the damage of the character
        /// </summary>
        /// <returns>Damage amount</returns>
        public abstract double CalculateDamage();

        /// <summary>
        /// Sum character base attributes from level with the extra attributes from items gaining
        /// </summary>
        public void CalculateTotalAttributes()
        {
            TotalAttributes.Strength = CharacterBaseAttributes.Strength + ExtraAttributes.Strength;
            TotalAttributes.Dexterity = CharacterBaseAttributes.Dexterity + ExtraAttributes.Dexterity;
            TotalAttributes.Intelligence = CharacterBaseAttributes.Intelligence + ExtraAttributes.Intelligence;
        }

        /// <summary>
        /// Equipt your character with a given armour
        /// </summary>
        /// <param name="armour">The armour you want to equipt</param>
        /// <returns>Message, if successful</returns>
        /// <exception cref="InvalidArmourException">If character level too low or armour not alllowed</exception>
        public string SetArmour(Armour armour)
        {
            if (Level < armour.RequiredLevel) //character level not enough, throw error
            {
                throw new InvalidArmourException($"You can't equipt this armour! Required level {armour.RequiredLevel}");
            }
            else if (!AllowedArmours.Contains(armour.ArmourType)) //character not allowed to equipt the item, throw error
            {
                throw new InvalidArmourException($"You are not able to equipt this armour!");

            }
            else if (Equipment.ContainsKey(armour.SlotType)) //replace slot with new armour
            {
                //remove attributes from old armour
                ExtraAttributes.Strength -= Equipment[armour.SlotType].ItemAttributes.Strength;
                ExtraAttributes.Dexterity -= Equipment[armour.SlotType].ItemAttributes.Dexterity;
                ExtraAttributes.Intelligence -= Equipment[armour.SlotType].ItemAttributes.Intelligence;
            }
            Equipment[armour.SlotType] = armour; //add or replace the armour to the slot and increase the extra attributes
            ExtraAttributes.Strength += armour.ItemAttributes.Strength;
            ExtraAttributes.Dexterity += armour.ItemAttributes.Dexterity;
            ExtraAttributes.Intelligence += armour.ItemAttributes.Intelligence;

            return "New armour equipped!";
        }

        /// <summary>
        /// Equipt your character with a given weapon
        /// </summary>
        /// <param name="weapon">The weapon you want to equipt</param>
        /// <returns>Message, if successful</returns>
        /// <exception cref="InvalidWeaponException">If character level too low or weapon not alllowed</exception>
        public string SetWeapon(Weapon weapon)
        {
            if (Level < weapon.RequiredLevel) ///character level not enough, throw error
            {
                throw new InvalidWeaponException($"You can't equipt this weapon! Required level {weapon.RequiredLevel}");
            }
            else if (!AllowedWeapons.Contains(weapon.WeaponType)) //character not allowed to equipt the item, throw error
            {
                throw new InvalidWeaponException($"You are not able to equipt this weapon!");

            }
            Equipment[SlotType.WEAPON] = weapon; //add or replace the weapon and set the weapon DPS
            WeaponDPS = weapon.CalculateDPS();

            return "New weapon equipped!";
        }

        /// <summary>
        /// Get the current stats of the character
        /// </summary>
        /// <returns>Message with stats</returns>
        public string GetStats()
        {
            CalculateTotalAttributes();
            return $"Character name: {Name}\nLevel: {Level}\nStrength: {TotalAttributes.Strength}\nDextery: {TotalAttributes.Dexterity}\nIntelligence:{TotalAttributes.Intelligence}\nDamage:{CalculateDamage()}%";
        }
    }
}
