﻿using RPG_Characters.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Character
{
    public class Ranger : Character
    {
        public Ranger(string name) : base(name)
        {
            CharacterBaseAttributes.Strength = 1;
            CharacterBaseAttributes.Dexterity = 7;
            CharacterBaseAttributes.Intelligence = 1;
            AllowedWeapons = new WeaponType[] { WeaponType.WEAPON_BOW };
            AllowedArmours = new ArmourType[] { ArmourType.ARMOUT_LEATHER, ArmourType.ARMOUR_MAIL };
        }

        public override double CalculateDamage()
        {
            return Math.Round(WeaponDPS * (1 + TotalAttributes.Dexterity / 100), 2);
        }

        public override void LevelUp()
        {
            base.LevelUp();
            CharacterBaseAttributes.IncreaseBaseAttributes(1, 5, 1);
        }
    }
}
