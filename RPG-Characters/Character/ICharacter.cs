﻿using RPG_Characters.Attributes;
using RPG_Characters.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Character
{
    public interface ICharacter
    {
        string Name { get; set; }
        int Level { get; set; }

        BasePrimaryAttributes CharacterBaseAttributes { get; set; }
        BasePrimaryAttributes ExtraAttributes { get; set; }
        BasePrimaryAttributes TotalAttributes { get; set; }
        WeaponType[] AllowedWeapons { get; set; }
        ArmourType[] AllowedArmours { get; set; }
        double WeaponDPS { get; set; }
        Dictionary<SlotType, Item> Equipment { get; set; }


        void LevelUp();

        double CalculateDamage();

        string SetWeapon(Weapon weapon);

        string SetArmour(Armour armour);

        string GetStats();

        void CalculateTotalAttributes();
    }
}
