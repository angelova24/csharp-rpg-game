﻿using RPG_Characters.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Character
{
    public class Warrior : Character
    {
        public Warrior(string name) : base(name)
        {
            CharacterBaseAttributes.Strength = 5;
            CharacterBaseAttributes.Dexterity = 2;
            CharacterBaseAttributes.Intelligence = 1;
            AllowedWeapons = new WeaponType[] { WeaponType.WEAPON_AXE, WeaponType.WEAPON_HAMMER, WeaponType.WEAPON_SWORD };
            AllowedArmours = new ArmourType[] { ArmourType.ARMOUR_MAIL, ArmourType.ARMOUR_PLATE };
        }

        public override double CalculateDamage()
        {
            return Math.Round(WeaponDPS * (1 + TotalAttributes.Strength / 100), 2);
        }

        public override void LevelUp()
        {
            base.LevelUp();
            CharacterBaseAttributes.IncreaseBaseAttributes(3, 2, 1);
        }
    }
}
