# RPG Characters

.NET Core Console Application

## Description

This project was created for learning purposes.\
Intended for learning OOP and unit testing.

### Project class diagram

 [Diagram](https://gitlab.com/angelova24/csharp-rpg-game/-/blob/main/ClassDiagram.jpg)


## Table of Contents

- [Getting started](#getting-started)
- [Installation](#installation)
- [Usage](#usage)
- [Authors and acknowledgment](#authors-and-acknowledgment)
- [Project status](#project-status)

## Getting started

1. Clone the repo
2. Open solution in Visual Studio
3. Run the unit tests


## Installation
Make sure you have installed at least the following tools:
```
• Visual Studio 2019
• .NET 5
```

## Usage
Simple class hierarchy with unit testing


## Authors and acknowledgment
@angelova24


## Project status
The project works perfectly and is completed taking in mind the project requirements.
